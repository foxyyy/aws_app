resource "aws_api_gateway_rest_api" "backend" {
  name = local.api_gw_name
}

resource "aws_api_gateway_resource" "backend_request" {
  parent_id   = aws_api_gateway_rest_api.backend.root_resource_id
  path_part   = "request"
  rest_api_id = aws_api_gateway_rest_api.backend.id
}

resource "aws_api_gateway_method" "backend_request_get" {
  authorization = "NONE"
  http_method   = "GET"
  resource_id   = aws_api_gateway_resource.backend_request.id
  rest_api_id   = aws_api_gateway_rest_api.backend.id
}

resource "aws_api_gateway_integration" "backend_request_get" {
  rest_api_id             = aws_api_gateway_rest_api.backend.id
  resource_id             = aws_api_gateway_resource.backend_request.id
  http_method             = aws_api_gateway_method.backend_request_get.http_method
  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.backend.invoke_arn
}

resource "aws_api_gateway_deployment" "backend" {
  rest_api_id = aws_api_gateway_rest_api.backend.id

  triggers = {
    redeployment = sha1(jsonencode([
      aws_api_gateway_resource.backend_request.id,
      aws_api_gateway_method.backend_request_get.id,
      aws_api_gateway_integration.backend_request_get.id,
    ]))
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_api_gateway_stage" "backend" {
  deployment_id = aws_api_gateway_deployment.backend.id
  rest_api_id   = aws_api_gateway_rest_api.backend.id
  stage_name    = "dev"
}