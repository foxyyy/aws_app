resource "aws_cloudwatch_log_group" "backend" {
  name              = "/aws/lambda/${local.backend_lambda_name}"
  retention_in_days = 14
}