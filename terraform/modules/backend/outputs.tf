output "api_gw_endpoint" {
  value = "${aws_api_gateway_stage.backend.invoke_url}${aws_api_gateway_resource.backend_request.path}"
}