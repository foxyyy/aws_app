resource "aws_iam_role" "backend" {
  name = local.backend_lambda_name

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "backend" {
  name = local.backend_lambda_name
  role = aws_iam_role.backend.id

  policy = data.aws_iam_policy_document.backend.json

}

data "aws_iam_policy_document" "backend" {
  statement {

    actions = [
      "logs:CreateLogStream",
      "logs:PutLogEvents",
    ]

    resources = [
      "${aws_cloudwatch_log_group.backend.arn}:*",
    ]
  }


}