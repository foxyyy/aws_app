resource "aws_s3_bucket" "spa" {
  bucket = local.spa_bucket_name
  force_destroy = var.s3_force_destroy
  tags = var.tags
}

resource "aws_s3_bucket_acl" "spa" {
  bucket = aws_s3_bucket.spa.id
  acl    = "private"
}


resource "aws_s3_bucket_website_configuration" "spa" {
  bucket = aws_s3_bucket.spa.bucket

  index_document {
    suffix = "index.html"
  }

  error_document {
    key = "error.html"
  }
}

resource "aws_s3_bucket_versioning" "versioning_spa" {
  bucket = aws_s3_bucket.spa.id
  versioning_configuration {
    status = "Enabled"
  }
}
