
module "spa" {
  source = "./../../modules/spa"

  env              = var.env
  s3_force_destroy = true
  api_endpoint     = module.backend.api_gw_endpoint
  acm_cert_id      = "c65d28b2-6abf-4da5-accc-5f445e92a06c"
  domain_name      = "ynastassia.sbs"

  tags = var.tags
}

module "backend" {
  source = "./../../modules/backend"

  env              = var.env
  build_version    = var.build_version
  source_s3_bucket = var.source_s3_bucket
  source_s3_key    = var.source_s3_key

  tags = var.tags
}

module "alerts" {
  source = "./../../modules/alerts"

  env = var.env
  lambda_names = [
    "backend"
  ]
  sns_subscription_list = [
    "nastassiayuhan@gmail.com"
  ]
  tags = var.tags
}

# # generate cert domain namecheap
# #cloudwatch alerts
# # sns notifications
# # scripts to put to s3
