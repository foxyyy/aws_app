variable "env" {
  default = "dev"
}

variable "tags" {
  type = map(any)
  default = {
    env   = "dev"
    owner = "devops-team"
  }
}

variable "source_s3_bucket" {
  default = "hw43.terraform"
}

variable "source_s3_key" {
  default = "packages"
}

variable "build_version" {
  default = "0.0.1"
}