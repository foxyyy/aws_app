env=$1
aws_profile=$2

if [ -z "$env" ]; then
    echo "env name is empty. env - dev"
    env="dev"
fi
if [ -z "$aws_profile" ]; then
    echo "AWS profile name is empty. Use default"

fi

aws_region=$(aws configure get region)
aws_account_id=$(aws sts get-caller-identity --query "Account" --output text $aws_profile)
echo "AWS region: $aws_region\nAWS account_id: $aws_account_id"

cloudfront_distr_id=$(aws cloudfront list-distributions \
    --query "DistributionList.Items[*].{id:Id,origin:Origins.Items[0].Id}\
    [?origin=='${env}-${aws_region}-${aws_account_id}-spa.s3.${aws_region}.amazonaws.com'].id" \
    --output text $aws_profile)
echo "Cloudfront distribution id: $cloudfront_distr_id"
invalidation_id=$(aws cloudfront create-invalidation \
    --distribution-id $cloudfront_distr_id --paths "/*" $aws_profile \
    | grep -i Location | rev | cut -d '"' -f 2 | cut -d "/" -f 1 | rev)
echo "Cloudfront invalidation id: $invalidation_id"
echo "Cloudfront invalidation..."
aws cloudfront wait invalidation-completed --distribution-id $cloudfront_distr_id \
    --id $invalidation_id $aws_profile
echo "Completed"
