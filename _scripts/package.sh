build_number=$1
infra_s3_name=$2
aws_profile=$3

if [ -z "$build_number" ]; then
    echo "Build number is empty. Build number - latest"
    build_number="latest"
fi
if [ -z "$infra_s3_name" ]; then
    echo "Infra s3 bucket is empty. Use default hw43.terraform"
    infra_s3_name="hw43.terraform"
fi
if [ -z "$aws_profile" ]; then
    echo "AWS profile name is empty. Use default"

fi

root_location=$(pwd)
echo "Current location:\n$(pwd)"
echo "Create tmp folder"
mkdir tmp tmp/lambdas
echo "Going to lambdas dir"
cd app/lambdas
pwd
echo "List of lambda dirs:\n$(ls)"
for folder in $(ls)
do
    echo "Starting package $folder"
	cd $folder
	if [ -f "requirements.txt" ]; then
        echo "File requirements.txt exists. Pull dependencies"
        pip install -r requirements.txt -t .
    else
        echo "File requirements.txt does not exist."
    fi
    archive_name="${folder}_${build_number}.zip"
    echo "Starting compression $archive_name"
    zip -r $archive_name . -x requirements.txt
    echo "Copy $archive_name to tmp dir"
    cp -r $archive_name $root_location/tmp/lambdas
done
echo "Upload labmdas to s3\n$(ls $root_location/tmp/lambdas)"
aws s3 cp $root_location/tmp/lambdas/ s3://$infra_s3_name/packages/lambdas/ --recursive  --exclude "*" --include "*.zip" $aws_profile

echo "Starting package spa\nCreating tmp dir"
aws s3 cp $root_location/app/spa/ s3://$infra_s3_name/packages/app/spa_$build_number/ --recursive --include "*" $aws_profile
